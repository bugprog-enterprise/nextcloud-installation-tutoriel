// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Tuto: Installer Nextcloud sur Debian',
  tagline: 'Découvrez comment créer votre propre cloud personnel avec Nextcloud grâce à ce tutoriel d\'installation simple et détaillé. Suivez les étapes pas à pas pour configurer une instance Nextcloud sur votre propre serveur ou hébergement web.',
  favicon: 'img/favicon.ico',

  // Set the production url of your site here
  url: 'https://doc.bugprog.studio',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'BugProg Studio', // Usually your GitHub org/user name.
  projectName: 'Nextcloud Installation Tutoriel', // Usually your repo name.

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/bugprog-enterprise/nextcloud-installation-tutoriel/-/blob/main/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/nextcloud_social.png',
      navbar: {
        title: 'BugProg Studio',
        logo: {
          alt: 'BugProg Studio Logo',
          src: 'img/BugProg_logo.png',
          href: 'https://bugprog.studio'
        },
        items: [
          {
            type: 'docSidebar',
            sidebarId: 'tutorialSidebar',
            position: 'left',
            label: 'Installer Nextcloud',
          },
          {
            type: 'docsVersionDropdown',
            position: 'right',
            dropdownActiveClassDisabled: true,
          },
          {
            href: 'https://gitlab.com/bugprog-enterprise/nextcloud-installation-tutoriel',
            label: 'Gitlab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'light',
        links: [
          {
            title: 'Site internet',
            items: [
              {
                label: 'BugProg Studio',
                href: 'https://bugprog.studio',
              },
            ],
          },
          {
            title: 'Social',
            items: [
              {
                label: 'Twitter',
                href: 'https://twitter.com/BugProg_Nantes',
              },
            ],
          },
          {
            title: 'Plus',
            items: [
              {
                label: 'Gitlab',
                href: 'https://gitlab.com/bugprog-enterprise/nextcloud-installation-tutoriel',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} BugProg-Enterprise. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
        additionalLanguages: ['apacheconf']
      },
    }),
};

module.exports = config;
