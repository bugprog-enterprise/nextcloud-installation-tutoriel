---
sidebar_position: 9
---

# Problèmes connus

```
The database is missing some indexes. Due to the fact that adding 
indexes on big tables could take some time they were not added 
automatically. By running "occ db:add-missing-indices" those missing 
indexes could be added manually while the instance keeps running. 
Once the indexes are added queries to those tables are usually much faster.
    - Missing index "calendarobject_calid_index" in table "oc_calendarobjects_props".
    - Missing index "schedulobj_principuri_index" in table "oc_schedulingobjects".
```

Exécutez la commande suivante :

```bash
sudo -u www-data php /votre/dossier/nextcloud/occ db:add-missing-indices
```

---

```
Some columns in the database are missing a conversion to big int.
Due to the fact that changing column types on big tables could take some
time they were not changed automatically. By running 'occ db:convert-filecache-bigint'
those pending changes could be applied manually. This operation needs to
be made while the instance is offline. For further details read the 
documentation page about this.
    mounts.storage_id
    mounts.root_id
    mounts.mount_id
```

Exécutez la commande suivante :

```bash
sudo -u www-data php /votre/dossier/nextcloud/occ db:convert-filecache-bigint
```

---

```
The PHP memory limit is below the recommended value of 512MB.
```

Editer le fichier suivant /etc/php/7.3/Apache2/php.ini, chercher la ligne `memory_limit` (CTRL + w avec nano) puis remplacer la valeur par 512M.

Redémarrer Apache avec :

```bash
sudo systemctl restart apache2
```

---

```
Module php-imagick in this instance has no SVG support. For better compatibility it is recommended to install it.
```

Exécutez la commande suivante :

```bash
sudo apt install libmagickcore-6.q16-6-extra
```

---

```
Your installation has no default phone region set. 
This is required to validate phone numbers in the profile settings
 without a country code. To allow numbers without a country code, 
please add "default_phone_region" with the respective ISO 3166-1 code ↗ 
of the region to your config file.
```

Editer le fichier suivant /var/www/nextcloud/config/config.php, et ajouter cette ligne :

`'default_phone_region' => 'FR'`

---
