---
sidebar_position: 1
---

# Introduction

![Nextcloud](../static/img/logo.svg)

Nextcloud propose sa plate-forme de collaboration de contenu open source et auto-hébergée, 
combinant ce qu'il décrit comme une interface utilisateur facile pour les solutions de cloud 
computing grand public avec les mesures de sécurité et de conformité dont les entreprises ont besoin.
Nextcloud offre un accès universel aux données via des interfaces mobiles, de bureau et web, ainsi
que des fonctions de communication et de collaboration sécurisées de nouvelle génération sur site, comme l'édition
de documents en temps réel, le chat et les appels vidéos, ce qui les place sous le contrôle direct de l'informatique 
et les intègre à l'infrastructure existante.

**Site officiel :** [www.nextcloud.com](https://nextcloud.com)

**Documentation officiel :** <https://docs.nextcloud.com/server/latest/admin_manual/>

## Prérequis :

👷‍♂️ Difficulté : moyen

🕛️ Temps : 1h

* 🖥️ Un ordinateur/Raspberry Pi 4b
* 💾 Une carte SD/HDD/SSD de 16 Go ou plus
* 🍓 [Raspberry OS](https://www.raspberrypi.com/software/) déjà installé ou [Debian 12](https://www.debian.org/download)
* 💿️ Une partition qui sera allouée au dossier data de nextcloud. Cela permet de faciliter l'ajout de HDD en cas de manque d'espace. (optionnelle)
* ☕️ Deux ou trois tasses de café
* 📚️ Il peut être intéressant de lire la [dernière section](/docs/going_further) en premier 😁

Bien, maintenant que tout ça est prêt, nous pouvons commencer.
