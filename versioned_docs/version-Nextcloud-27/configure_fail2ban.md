---
sidebar_position: 9
---

# Configurer Fail2ban (optionnelle)


Si vous décidez de rendre accessible votre serveur depuis l'extérieur, il est fortement probable qu'il devienne une cible pour des personnes malveillantes.
Pour se prémunir contre d'éventuelles attaques de ce type, nous allons utiliser le logiciel Fail2ban qui utilise les tables iptables pour bannir les éventuelles IPs qui chercheraient à se connecter de manière répétée et avec sans succès.
Voici un lien pour plus d'informations : https://doc.ubuntu-fr.org/fail2ban.

Dans un premier temps, nous devons installer le logiciel :

```bash
sudo apt install fail2ban
```

Nous allons maintenant créer le filtre de configuration propre à nextcloud :

```bash
nano /etc/fail2ban/filter.d/nextcloud.conf
```

Et y mettre ceci :

```bash
[Definition]
_groupsre = (?:(?:,?\s*"\w+":(?:"[^"]+"|\w+))*)
failregex = ^\{%(_groupsre)s,?\s*"remoteAddr":"<HOST>"%(_groupsre)s,?\s*"message":"Login failed:
            ^\{%(_groupsre)s,?\s*"remoteAddr":"<HOST>"%(_groupsre)s,?\s*"message":"Trusted domain error.
datepattern = ,?\s*"time"\s*:\s*"%%Y-%%m-%%d[T ]%%H:%%M:%%S(%%z)?"
```

Cela permet de définir les règles de bannissement pour identifier les authentifications ratées depuis les logs de nextcloud. Cela s'applique pour l'interface Web, WebDav et les sous domaine non autorisés.

Nous devons maintenant créer un autre fichier pour définir les règles du bannissement.

```bash
nano /etc/fail2ban/jail.d/nextcloud.conf
```

Pour y mettre :

```bash
[nextcloud]
backend = auto
enabled = true
port = 80,443
protocol = tcp
filter = nextcloud
maxretry = 3
bantime = 86400
findtime = 43200
logpath = /var/www/nextcloud/data/nextcloud.log
```

Ici, rien à modifier, sauf si vous utilisez différents ports. Le **bantime** est le temps que le serveur sera indisponible pour l'attaquant, ici 86400s soit 24h.
Le **findtime** correspond au nombre d'essais maximum (maxretry) sur la period défini.

Nous pouvons appliquer les changements avec :

```bash
sudo systemctl restart fail2ban
```

Bien, nous pouvons vérifier que tout fonctionne correctement avec :

```bash
fail2ban-client status nextcloud
```

Nous en avons fini avec fail2ban, il est temps de nous rendre sur notre serveur depuis le navigateur.
