---
sidebar_position: 10
---

# Aller plus loin

### Nextcloud depuis l'extérieur

Avoir accès à votre serveur Nextcloud en local, c'est bien, mais depuis l'extérieur, c'est mieux. :)

Ce n'est pas ici l'objet de ce tuto de vous montrer comment faire, 
car les manipulations varient en fonction de votre opérateur et des box. 
Une chose est sûre, c'est qu'il faut ouvrir les ports 80/tcp et 443/tcp dans les tables NAT/PAT, 
avoir une IP statique afin d'éviter de se connecter chez le voisin à chaque fois que votre box redémarre 
(c'est un service qui peut être payant selon votre opérateur) et n'oubliez pas d'autoriser l'IP dans le fichier de config.php de votre serveur Nextcloud.

### Toujours plus d'espace

Même s'il existe des cartes SD de 250 Go, un jour, vous vous sentirez sûrement à l'étroit. 
Il existe des outils comme mhddfs qui vont vous permettre de fusionner les partitions entre-elles. 
Je vous renvoie vers ce tuto pour plus d'informations : [Mhddfs - Combine Several Smaller Partition into One Large Virtual Storage](https://www.tecmint.com/combine-partitions-into-one-in-linux-using-mhddfs/)
