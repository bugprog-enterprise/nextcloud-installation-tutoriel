---
sidebar_position: 5
---


# Configurer le cache

Pour que Nextcloud soit plus performant, il est recommandé de configurer une mémoire cache. Elle permet de faciliter l'accès aux fichiers dans le temps.

Pour cela, il suffit d'installer le module php suivant : `php-apcu`

:::caution

Par défaut apcu CLI n'est pas activé il nous faut rajouter : **apc.enable_cli=1** dans le fichier `/etc/php/8.2/mods-available/apcu.ini`

:::


Ajoutons cette ligne `'memcache.local' => '\OC\Memcache\APCu',` dans le fichier **config.php** présent dans la racine de votre installation dans le dossier config, soit `/var/www/nextcloud/config/config.php` dans notre cas.

Le fichier **config.php** est un fichier de configuration, c'est là où tous les paramètres de Nextcloud comme la base de donnée utilisé, le serveur SMTP pour l'envoi d'email et encore plein d'autres paramètres y sont stockés.

Puis `sudo systemctl restart apache2` pour appliquer.
