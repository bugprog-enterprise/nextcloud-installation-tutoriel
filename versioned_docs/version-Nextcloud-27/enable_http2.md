---
sidebar_position: 8
---

# Activer HTTP2 (optionnelle)

Les principaux objectifs de HTTP/2 sont de réduire la latence en permettant le multiplexage complet des demandes et 
des réponses, de minimiser la surcharge du protocole par une compression efficace des champs d'en-tête HTTP et d'ajouter 
la prise en charge de la hiérarchisation des demandes et du push serveur.
Il existe un grand nombre d'autres améliorations du protocole, telles que de 
nouveaux mécanismes de contrôle de flux, de traitement des erreurs et de mise à niveau.

Source:
[Introduction to HTTP/2](https://developers.google.com/web/fundamentals/performance/http2?hl=en)

Dans un premier temps, nous devons désactiver quelques modules qui ne sont pas compatibles avec HTTP2.

```shell
sudo a2dismod php8.2
```

Par défaut, Apache utilise le module MPM (module multi-processus) prefork. 
Ce MPM (module multi-processus) n'est pas compatible avec HTTP/2, nous devons donc le remplacer par le module mpm_event, plus moderne.

```shell
sudo a2dismod mpm_prefork
sudo a2enmod mpm_event
```

```shell
sudo apt install php8.2-fpm
sudo a2enconf php8.2-fpm
sudo a2enmod proxy_fcgi
```

Et enfin :
``` shell
sudo a2enmod http2
sudo systemctl restart apache2
```

Si vous obtenez une erreur essayé de redémarrer votre Raspberry Pi.

### Explication des modules : prefork et event

#### Prefork

Ce module multi-processus (MPM) implémente un serveur web avec démarrage anticipé de processus. Chaque processus du serveur peut répondre aux requêtes entrantes, et un processus parent contrôle la taille du jeu de processus enfants. C'est également le MPM le plus approprié si l'on veut isoler les requêtes les unes des autres, de façon à ce qu'un problème concernant une requête n'affecte pas les autres.

Source: [prefork - Serveur HTTP Apache Version 2.4](https://httpd.apache.org/docs/current/fr/mod/prefork.html)

#### Event

Le module multi-processus (MPM) event est conçu pour permettre le traitement d'un nombre accru de requêtes simultanées en déléguant certaines tâches aux threads d'écoute, libérant par là-même les threads de travail et leur permettant de traiter les nouvelles requêtes.

Source: [event - Serveur HTTP Apache Version 2.4](https://httpd.apache.org/docs/2.4/fr/mod/event.html)
