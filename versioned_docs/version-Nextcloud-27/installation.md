---
sidebar_position: 2
---

# Installation

La première chose que nous devons faire c'est installer un serveur Web.
C'est lui qui va entrer en contact avec le navigateur du client.
Il est là pour répondre aux requêtes qu'on lui donne.
Il en existe une petite dizaine, mais les deux principaux sont :

* Apache
* Nginx

Nginx est connue pour sa capacité à gérer un grand nombre de connexions, ce qui l'a rendu très populaire, il est d'ailleurs utilisé par la plupart des serveurs qui reçoivent plusieurs milliers de requêtes par seconde.
Apache est le serveur Web le plus utilisé et personnalisable notamment avec les fichiers .htaccess.

Dans ce tutoriel, nous utiliserons Apache comme serveur Web pour des soucis de facilité.

Nous avons aussi besoin d'une base de donnée pour y stocker des informations comme les utilisateurs et les événements du calendrier... Les plus connues sont Mysql et Mariadb.

Dans ce tutoriel, nous utiliserons Mariadb mais vous pouvez prendre Mysql si vous le désirez, les commandes étant 100% compatibles entre les deux logiciels.

Nous aurons aussi besoin de php, là rien à dire dessus.

Nous allons commencer par installer la base de donnée et le serveur Web :

```shell
sudo apt install apache2 mariadb-server libApache2-mod-php8.2 openssl
```

Puis php et quelques modules :

```shell
sudo apt install php8.2-gd php8.2-mysql php8.2-curl php8.2-mbstring php8.2-intl
```

```shell
sudo apt install php8.2-gmp php8.2-bcmath php-imagick php8.2-xml php8.2-zip
```

Nous allons vérifier qu'Apache s'est bien installé. Pour cela, ouvrez votre navigateur et tapez dans l'URL l'IP de votre serveur. Une page comme celle-ci devrait apparaître :

![Apache Demo page](./static/img/Apache-demo.png)

Si ce n'est pas le cas, essayez de démarrer Apache avec `sudo systemctl start apache2`

Une fois installé, nous allons récupérer les fichiers Nextcloud.

Il suffit de se rendre sur cette page : [Install - Nextcloud](https://nextcloud.com/install/#instructions-server) et de faire un clic droit (sur Get ZIP file) puis copier l'adresse lien.


![Nextcloud_Download_page](./static/img/Nextcloud_Download_Page.png)


Dans un terminal du serveur, nous pouvons lancer la commande suivante :

```shell
wget https://download.nextcloud.com/server/releases/latest.zip
```

Le fichier va alors se télécharger dans le répertoire où vous êtes.

Le fichier actuellement compressé est inutilisable, nous devons donc le décompresser et le changer de répertoire.

```shell
sudo unzip latest.zip -d /var/www/
```

La commande unzip avec l'option -d permet d'extraire le fichier dans le répertoire donné, ici : **/var/www/** qui est le dossier réservé aux sites-Web. Vous pouvez, si vous le souhaitez changer de répertoire, mais je vous recommande ce répertoire.

Maintenant que notre fichier est prêt, nous devons nous assurer qu'Apache puisse y avoir accès avec la commande suivante :

```shell
sudo chown -R www-data:www-data /var/www/nextcloud
```
