---
sidebar_position: 4
---

# Configurer MariaDB

Nous allons commencer par terminer la configuration de mariadb avec la commande ci-dessous :

```sql
sudo mysql_secure_installation
```

Mysql va alors nous poser tout un tas de question auquel il va falloir répondre.

**Set root password? [Y/n] :**

```
Y
```

**Remove anonymous users? [Y/n] :**

```
Y
```

**Disallow root login remotely? [Y/n] :**

```
Y
```

**Remove test database and access to it? [Y/n] :**

```
Y
```

**Reload privilege tables now? [Y/n] :**

```
Y
```

Voici le résumé des réponses :

```
Set root password? [Y/n] Y
New password: 
Re-enter new password: 
 ... Success!

Remove anonymous users? [Y/n] Y
 ... Success!

Disallow root login remotely? [Y/n] Y
 ... Success!

Remove test database and access to it? [Y/n] Y
 ... Success!

Reload privilege tables now? [Y/n] Y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

Nous avons aussi besoin de créer un autre utilisateur spécifique à Nextcloud. Rentrons pour cela dans mysql en rentrant simplement : `sudo mysql` Puis créer l'utilisateur avec :

```sql
CREATE USER 'nextcloud_user'@'localhost' IDENTIFIED BY 'password';
```

Bien sûr, n'oubliez pas de changer de mot de passe :)

Puis la base de donnée :

```sql
CREATE DATABASE IF NOT EXISTS nextcloud_db CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
```

Puis, nous donnons les droits à notre utilisateur Nextcloud :

```sql
GRANT ALL PRIVILEGES ON nextcloud_db.* TO 'nextcloud_user'@'localhost';
```

On applique les changements :

```sql
FLUSH PRIVILEGES;
```

Bien maintenant, il ne reste plus qu'à nous connecter sur notre site Web :)

Rentrer l'adresse IP de votre serveur dans votre navigateur et... magie cette page devrait apparaître :

Si c'est le cas bravo sinon c'est que vous avez loupé un truc. :)

![alt text](./static/img/Nextcloud-config.png)

Là, rien de plus simple.
Entrer votre nouvel identifiant et mot de passe (Ce compte est un compte administrateur, ne l'utilisez pas comme un compte personnel !!)

Pour le dossier data deux possibilités :

Soit vous avez crée une autre partition pour le dossier data, dans ce cas changez le répertoire du dossier (N'oubliez pas de donner les droits à Apache avec `sudo chown -R www-data:www-data /votre/partition` !!) ou sinon vous pouvez laisser par défaut.

Pour le reste, mettez les infos qu'il nous demande :

```
Utilisateur de la base de donnée : nextcloud_user
Mot de passe de l'utilisateur : password
Nom de la base de donnée : nextcloud_db
```

Ce n'est là qu'un exemple, mettez les informations que vous avez définies plus haut !!
Une fois complété, vous pouvez valider.
Remarquez que Nextcloud vous demande si vous voulez installer des plugins comme le calendrier, only-office et autre, à vous de choisir.

Roulement de tambour................

![alt text](./static/img/nextcloud_welcome.png)

OUI !!!!!

Cependant, ce n'est que le commencement :)
