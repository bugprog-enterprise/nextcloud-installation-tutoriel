---
sidebar_position: 7
---

# Signer le certificat (optionnelle)

Bien, maintenant que votre serveur fonctionne à merveille, vous voudriez peut-être le rendre plus facile d'accès. Par exemple, quand vous vous connectez, autoriser à chaque fois le
certificat peut être redondant, dans ce cas la certification du
certificat s'impose. Ce n'est pas le but de ce tuto de vous montrer comment faire, mais il y a des solutions comme Let's encrypt qui le propose gratuitement.

Je vous renvoie vers le tuto d'Alban qui l'explique très bien : [Activation du module SSL/TLS](https://trevilly.com/installation-de-nextcloud-21-avec-apache-php-et-mariadb/#SSL)

:::caution

Vous devez avoir un nom de domaine au préalable !!

:::
