---
sidebar_position: 3
---


# Configurer Apache

Nous devons commencer par activer quelques modules nécessaires au bon fonctionnement de Nextcloud.

```
sudo a2enmod rewrite
sudo a2enmod headers
sudo a2enmod env
sudo a2enmod dir
sudo a2enmod mime
```

Petite explication de chaque module :

* **rewrite** : Permet de modifier les URLs.
* **headers** : Permet de modifier les en-têtes des requêtes et des réponses HTTP.
* **env** : Permet de modifier les variables propres à Apache.
* **dir** : Permet la redirection d'URL qui se termine avec un répertoire sans slash.
* **mime** : Associe des métadonnées à chaque type de fichier.

Une fois les modules activés, nous pouvons nous occuper de la configuration d'Apache.

Les fichiers de configurations d'Apache se trouvent généralement dans /etc/apache2/

Nous allons éditer le fichier suivant `/etc/apache2/sites-available/000-default.conf` mais avant de faire cela, nous allons le renommer, car `000-default.conf` ne veut pas dire grand-chose.

```shell
sudo mv /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/nextcloud.conf
sudo nano /etc/apache2/sites-available/nextcloud.conf
```

```apacheconf
<VirtualHost *:80>
    # The ServerName directive sets the request scheme, hostname and port that
    # the server uses to identify itself. This is used when creating
    # redirection URLs. In the context of virtual hosts, the ServerName
    # specifies what hostname must appear in the request's Host: header to
    # match this virtual host. For the default virtual host (this file) this
    # value is not decisive as it is used as a last resort host regardless.
    # However, you must set it for any further virtual host explicitly.
    #ServerName www.example.com

    ServerAdmin Webmaster@localhost
    DocumentRoot /var/www/html

    # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
    # error, crit, alert, emerg.
    # It is also possible to configure the loglevel for particular
    # modules, e.g.
    #LogLevel info ssl:warn

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    # For most configuration files from conf-available/, which are
    # enabled or disabled at a global level, it is possible to
    # include a line for only one particular virtual host. For example the
    # following line enables the CGI configuration for this host only
    # after it has been globally disabled with "a2disconf".
    #Include conf-available/serve-cgi-bin.conf
</VirtualHost>

# vim: syntax=Apache ts=4 sw=4 sts=4 sr noet
```

La balise `<VirtualHost *:80>` spécifie à Apache d'écouter sur le port 80 (http).

Je vous laisse décommenter la ligne suivante : `ServerName www.example.com` et mettre votre nom de domaine si vous en avez un où votre IP dans le cas contraire.

`ServerAdmin` Correspond à votre email. `DocumentRoot` est le répertoire de nos fichiers Web, ici c'est /var/www/nextcloud/

Vous devez donc avoir un fichier qui ressemble à cela :

```apacheconf
<VirtualHost *:80>
    # The ServerName directive sets the request scheme, hostname and port that
    # the server uses to identify itself. This is used when creating
    # redirection URLs. In the context of virtual hosts, the ServerName
    # specifies what hostname must appear in the request's Host: header to
    # match this virtual host. For the default virtual host (this file) this
    # value is not decisive as it is used as a last resort host regardless.
    # However, you must set it for any further virtual host explicitly.

    ServerName www.domaine.com

    ServerAdmin admin@domaine.com
    DocumentRoot /var/www/nextcloud

    # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
    # error, crit, alert, emerg.
    # It is also possible to configure the loglevel for particular
    # modules, e.g.
    #LogLevel info ssl:warn

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    # For most configuration files from conf-available/, which are
    # enabled or disabled at a global level, it is possible to
    # include a line for only one particular virtual host. For example the
    # following line enables the CGI configuration for this host only
    # after it has been globally disabled with "a2disconf".
    #Include conf-available/serve-cgi-bin.conf
</VirtualHost>

# vim: syntax=Apache ts=4 sw=4 sts=4 sr noet
```

Nous avons aussi besoin de rajouter quelques petites règles supplémentaires pour la sécurité de notre serveur qui s'appliquera sur le port 80(http) et 443(https). Pour cela, nous allons rajouter quelques lignes au début de notre fichier de configuration.

```apacheconf
Alias /nextcloud "/var/www/nextcloud/"

<Directory /var/www/nextcloud/>
  Require all granted
  AllowOverride All
  Options FollowSymLinks MultiViews

  <IfModule mod_dav.c>
    Dav off
  </IfModule>

</Directory>
```

**Penser à modifier les valeurs en fonction de votre configuration.**

```apacheconf
Require all granted
```

Permet d'autoriser toutes les IP à s'y connecter.

```apacheconf
AllowOverride All
```

Indique qu'il y a des fichiers de configuration externe ( fichier .htaccess) qui sont présents et qu'Apache doit les lire.

Votre fichier doit ressembler à cela (j'ai supprimé les commentaires pour plus de visibilité.):

```apacheconf
Alias /nextcloud "/var/www/nextcloud/"

<Directory /var/www/nextcloud/>
  Require all granted
  AllowOverride All
  Options FollowSymLinks MultiViews

  <IfModule mod_dav.c>
    Dav off
  </IfModule>

</Directory>

<VirtualHost *:80>

    ServerName www.domaine.com

    ServerAdmin admin@domaine.com
    DocumentRoot /var/www/nextcloud

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

# vim: syntax=Apache ts=4 sw=4 sts=4 sr noet
```

Puis, nous activons notre configuration et redémarrons le service Apache pour appliquer les changements :

```shell
sudo a2ensite nextcloud.conf #Permet d'activer notre fichier de configuration
sudo systemctl restart apache2 #Redémarre Apache
```

Maintenant que la configuration d'Apache est fini (pour le moment) nous pouvons nous attaquer à mariadb :)
